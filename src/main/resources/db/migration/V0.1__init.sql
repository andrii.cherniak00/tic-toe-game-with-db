CREATE TABLE IF NOT EXISTS account (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  email VARCHAR(45) NOT NULL,
  password VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX email_UNIQUE (email ASC) VISIBLE,
  UNIQUE INDEX name_UNIQUE (name ASC) VISIBLE
);

CREATE TABLE IF NOT EXISTS game (
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  start_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  screen BLOB,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS game_session (
  game_id BIGINT UNSIGNED NOT NULL,
  account_id BIGINT UNSIGNED NOT NULL,
  game_result ENUM('WIN', 'LOSE', 'DRAW'),
  PRIMARY KEY (account_id, game_id),
  INDEX fk_game_session_account_idx (account_id ASC) VISIBLE,
  INDEX fk_game_session_game1_idx (game_id ASC) VISIBLE,
  CONSTRAINT fk_game_session_account
    FOREIGN KEY (account_id)
    REFERENCES account (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_game_session_game1
    FOREIGN KEY (game_id)
    REFERENCES game (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    );
