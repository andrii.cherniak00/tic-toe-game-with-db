package com.tictactoe.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    @Column(name = "start_time")
    private Timestamp timestamp;
    private byte[] screen;
    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
    private List<GameSession> gameSessionList;

}
