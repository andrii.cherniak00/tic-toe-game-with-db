package com.tictactoe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tictactoe.enums.GameResult;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table
public class GameSession {
    @EmbeddedId
    private GameSessionId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("account_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JsonIgnore
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("game_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JsonIgnore
    private Game game;

    @Enumerated(EnumType.STRING)
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "game_result")
    private GameResult gameResult;

    public GameSession(Account account, Game game, GameResult gameResult) {
        this.account = account;
        this.game = game;
        this.gameResult = gameResult;
        this.id = new GameSessionId(account.getId(), game.getId());
    }
}