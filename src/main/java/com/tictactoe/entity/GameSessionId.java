package com.tictactoe.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class GameSessionId implements Serializable {
    @Column(name = "account_id")
    private Long accountId;
    @Column(name = "game_id")
    private Long gameId;
}
