package com.tictactoe.dto;

import com.tictactoe.enums.GameResult;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GameSessionDto {
    private Long accountId;
    private Long gameId;
    private GameResult gameResult;
}
