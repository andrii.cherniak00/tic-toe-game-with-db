package com.tictactoe.controller;

import com.tictactoe.entity.Account;
import com.tictactoe.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/account")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(
            value = "/{id}",
            produces = "application/json",
            method = RequestMethod.GET)
    public Account findAccountById(@PathVariable Long id) {
        return accountService.getAccount(id);
    }

    @RequestMapping(
            value = "/all",
            produces = "application/json",
            method = RequestMethod.GET)
    public List<Account> getAllUsers() {
        return accountService.getAllUser();
    }

    @RequestMapping(
            value = "/registration",
            consumes = "application/json",
            produces = "application/json",
            method = RequestMethod.POST)
    public ResponseEntity<Account> createNewAccount(@RequestBody Account account) {
        return ResponseEntity.ok(accountService.createNewAccount(account));
    }

    @RequestMapping(
            value = "/login",
            consumes = "application/json",
            produces = "application/json",
            method = RequestMethod.POST)
    public ResponseEntity<Account> login(@RequestBody Account account) {
        return ResponseEntity.ok(accountService.loadAccount(account));
    }


    @RequestMapping(
            value = "/",
            method = RequestMethod.GET)
    public String index() {
        return "Hello!";
    }

}
