package com.tictactoe.controller;

import com.tictactoe.dto.GameSessionDto;
import com.tictactoe.entity.Account;
import com.tictactoe.entity.Game;
import com.tictactoe.entity.GameSession;
import com.tictactoe.enums.GameResult;
import com.tictactoe.service.AccountService;
import com.tictactoe.service.GameService;
import com.tictactoe.service.GameSessionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/session")
public class GameSessionController {
    private final GameSessionService gameSessionService;
    private final AccountService accountService;
    private final GameService gameService;

    public GameSessionController(GameSessionService gameSessionService, AccountService accountService, GameService gameService) {
        this.gameSessionService = gameSessionService;
        this.accountService = accountService;
        this.gameService = gameService;
    }

    @RequestMapping(
            value = "/all",
            produces = "application/json",
            method = RequestMethod.GET)
    public List<GameSession> getAllGames() {
        return gameSessionService.getAllSessions();
    }

    @RequestMapping(
            value = "/",
            consumes = "application/json",
            produces = "application/json",
            method = RequestMethod.POST)
    public ResponseEntity<GameSession> createNewSession(@RequestBody GameSessionDto session) {
        Account account = accountService.getAccount(session.getAccountId());
        Game game = gameService.getGameById(session.getGameId());
        GameResult gameResult = session.getGameResult();
        return ResponseEntity.ok(gameSessionService.createSession(account, game, gameResult));
    }
}
