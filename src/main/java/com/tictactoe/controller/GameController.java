package com.tictactoe.controller;

import com.tictactoe.entity.Game;
import com.tictactoe.service.GameService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/game")
public class GameController {
    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(
            value = "/{id}",
            produces = "application/json",
            method = RequestMethod.GET)
    public Game findAccountById(@PathVariable Long id) {
        return gameService.getGameById(id);
    }

    @RequestMapping(
            value = "/all",
            produces = "application/json",
            method = RequestMethod.GET)
    public List<Game> getAllGames() {
        return gameService.getAllGames();
    }

    @RequestMapping(
            value = "/",
            consumes = "application/json",
            produces = "application/json",
            method = RequestMethod.POST)
    public ResponseEntity<Game> createNewAccount(@RequestBody Game game) {
        return ResponseEntity.ok(gameService.createNewGame(game));
    }

}
