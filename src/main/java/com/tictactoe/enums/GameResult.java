package com.tictactoe.enums;

public enum GameResult {
    WIN,
    LOSE,
    DRAW
}
