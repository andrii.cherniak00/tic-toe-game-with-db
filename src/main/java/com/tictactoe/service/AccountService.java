package com.tictactoe.service;

import com.tictactoe.entity.Account;
import com.tictactoe.repo.AccountRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final PasswordEncoder passwordEncoder;

    public AccountService(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<Account> getAllUser() {
        return accountRepository.findAll();
    }

    public Account getAccount(Long id) {
        return accountRepository.findById(id).orElse(new Account());
    }

    public Account createNewAccount(Account account) {
        return accountRepository.save(account);
    }

    public Account loadAccount(Account account) {
        return accountRepository
                .findByName(account.getName())
                .filter(acc -> passwordEncoder
                        .matches(account.getPassword(), acc.getPassword()))
                .orElseThrow(RuntimeException::new);
    }
}
