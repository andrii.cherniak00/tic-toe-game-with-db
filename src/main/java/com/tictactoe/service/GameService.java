package com.tictactoe.service;

import com.tictactoe.entity.Game;
import com.tictactoe.repo.GameRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService {
    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Game getGameById(Long id) {
        return gameRepository.findById(id).orElse(new Game());
    }

    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    public Game createNewGame(Game game) {
        return gameRepository.save(game);
    }
}
