package com.tictactoe.service;

import com.tictactoe.entity.Account;
import com.tictactoe.entity.Game;
import com.tictactoe.entity.GameSession;
import com.tictactoe.enums.GameResult;
import com.tictactoe.repo.GameSessionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GameSessionService {
    private final GameSessionRepository gameSessionRepository;

    public GameSessionService(GameSessionRepository gameSessionRepository) {
        this.gameSessionRepository = gameSessionRepository;
    }

    public List<GameSession> getAllSessions() {
        return gameSessionRepository.findAll();
    }


    public GameSession createSession(Account account, Game game, GameResult result) {
        return Optional.of(gameSessionRepository.save(new GameSession(account, game, result))).orElse(new GameSession());
    }
}
